/**
 * @file i32_circulant.h
 * @brief C include file, circulant matrix definitions
 * @author fisherprime
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef _I32_CIRCULANT_H
#define _I32_CIRCULANT_H

// Necessary headers
// ////////////////////////////////
#include <stdint.h>

#include "types/vector/i32/i32_vector.h"
// ////////////////////////////////

// Return statuses
// ////////////////////////////////
enum circ_return_codes {
	ERR_CIRC_MATRIX = (int8_t)(Z_VECTOR + 1), // Circulant matrix pointer
	// references invalid memory
	ERR_INPUT_VECTOR // Vector pointer references
	// invalid memory
};
// ////////////////////////////////

// Definitions
// ////////////////////////////////
// ////////////////////////////////

// Typedefs
// ////////////////////////////////
// ////////////////////////////////

// Function prototypes: Circulant operations
// ////////////////////////////////
int8_t i32_circulant_create(i32_vector *matrix, i32_vector *vec);
int8_t i32_circulant_print(i32_vector *matrix);
int8_t i32_circulant_rotate_matrix_rows(i32_vector *matrix, size_t dimensions);
// ////////////////////////////////

// Inline functions
// ////////////////////////////////
// ////////////////////////////////

// Extern functions
// ////////////////////////////////
// ////////////////////////////////

// Global variables
// ////////////////////////////////
// ////////////////////////////////

// Macros
// ////////////////////////////////
#define GENERATE_CIRCULANT(buff, in_vec, str_len, circ_mtx, len, out_str)      \
	__extension__({                                                        \
		printf("\n[+] Client vector received\n");                      \
		to_vector(&buff, in_vec, (size_t)str_len);                     \
		create_circulant(circ_mtx, in_vec);                            \
		len = to_string(&out_str, circ_mtx);                           \
	})
// ////////////////////////////////

#endif // _I32_CIRCULANT_H
