/**
 * @file i32_vector.h
 * @brief C include file, int32_t vector definitions
 * @author fisherprime
 * @date 2019-09-18
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef _I32_VECTOR_H
#define _I32_VECTOR_H

// Necessary headers
// ////////////////////////////////
#include <stdint.h>
#include <sys/types.h>

#if USE_FLATCC == 1
#ifndef NDEBUG
#pragma message "Flatbuffers enabled"
#endif

/**
 * @headerfile Flatbuffers vector serializer
 */
#include "./i32_vector_builder.h"

/**
 * @headerfile Flatbuffers vector deserializer
 */
#include "./i32_vector_reader.h"
#else
#ifndef NDEBUG
#pragma message "Flatbuffers disabled"
#endif
#endif

/**
 * @headerfile Flatbuffers vector verifier
 * @brief      Not in use due to undefined reference to flattcc_verify_field
 */
// #include "./i32_vector_verifier.h"
// ////////////////////////////////

// Return statuses 50-*
// ////////////////////////////////
enum vec_return_codes {
	// Flatbuffers
	ERR_FLATCC_PACK = (int8_t)-50,
	ERR_FLATCC_UNPACK = (int8_t)-51,

	// Vector stuff
	ERR_NO_VECTOR = (int8_t)-52, // Vector doesn't exist
	ERR_VECTOR_EMPTY = (int8_t)-53,
	ERR_VECTOR_BOUNDS = (int8_t)50,
	ERR_VECTOR_COPY,
	ERR_VECTOR_CREATE,
	ERR_VECTOR_ERASE,
	ERR_VECTOR_LOGGING_FREE,
	ERR_VECTOR_INIT,
	ERR_VECTOR_INSERT,
	ERR_VECTOR_POP,
	ERR_VECTOR_POPULATE,
	ERR_VECTOR_PUSH,
	ERR_VECTOR_REALLOC,
	ERR_VECTOR_RESERVE,
	ERR_VECTOR_SHRINK,

	Z_VECTOR // Hold upper limit for error codes
};
// ////////////////////////////////

// Definitions
// ////////////////////////////////

/**
 * Convenient namespace macro to manage long namespace prefix. Used with flatbuffers.
 */
#if USE_FLATCC == 1
#undef ns
#define ns(x) FLATBUFFERS_WRAP_NAMESPACE(MyC_Vector, x)
#endif

#define DEFAULT_VECTOR_SIZE (size_t)8

enum { EXPAND_VECTOR = 0, SHRINK_VECTOR = -1 };
// ////////////////////////////////

// Typedefs
// ////////////////////////////////
/**
 * @brief int32_t vector (dynamic array)
 */
typedef struct {
	size_t limit; // Size limit of the "data" array
	size_t size; // Number of elements in "data" array

	__int32_t *data;
} i32_vector;
// ////////////////////////////////

// Function prototypes: Vector operations
// ////////////////////////////////
// Element access
int32_t *i32_vector_at(i32_vector *vec, size_t position);
int32_t *i32_vector_rear(i32_vector *vec);
int32_t *i32_vector_data(i32_vector *vec);
int32_t *i32_vector_front(i32_vector *vec);

// Capacity
int8_t i32_vector_empty(i32_vector *vec);
int8_t i32_vector_reserve(i32_vector *vec, size_t new_size);
int8_t i32_vector_shrink(i32_vector *vec);
size_t *i32_vector_limit(i32_vector *vec);
size_t *i32_vector_size(i32_vector *vec);

// Modifiers
int8_t i32_vector_clear(i32_vector *vec);
int8_t i32_vector_init(i32_vector *vec, size_t vct_size);
int8_t i32_vector_copy(const i32_vector *vec, int32_t **duplicate,
		       size_t new_size);
int8_t i32_vector_details(i32_vector *vec);
int8_t i32_vector_erase(i32_vector *vec, size_t position);
int8_t i32_vector_free(i32_vector *vec);
int8_t i32_vector_insert(i32_vector *vec, size_t position, int32_t user_input);
int8_t i32_vector_cut_rear(i32_vector *vec);
int8_t i32_vector_cut_first(i32_vector *vec, int32_t user_input);
int8_t i32_vector_cut_last(i32_vector *vec, int32_t user_input);
int8_t i32_vector_push_rear(i32_vector *vec, int32_t user_input);
int8_t i32_vector_resize(i32_vector *vec, int32_t new_size);
int8_t i32_vector_set(i32_vector *vec, size_t position, int32_t user_input);

// Type conversion
int8_t to_vector(char **serialized_vec, i32_vector *vec, size_t len);
size_t to_string(i32_vector *vec, char **serialized_vec);
// ////////////////////////////////

// Extern functions
// ////////////////////////////////
// extern int32_t string_exist (char *string);
// ////////////////////////////////

// Global variables
// ////////////////////////////////
// ////////////////////////////////

// Macros
// ////////////////////////////////
// Could used a static inline function
#define VECTOR_EXIST_ERROR(v)                                                  \
	({                                                                     \
		print_error(                                                   \
			NULL, 0,                                               \
			(char *)"%s: Referenced vector [%s] doesn't exist\n",  \
			__FUNCTION__, #v);                                     \
	}) // VECTOR_EXIST_ERROR

#define I32_VECTOR_EXIST(v)                                                    \
	__extension__({                                                        \
		if (!v) {                                                      \
			VECTOR_EXIST_ERROR(v);                                 \
			raise(SIGTERM);                                        \
		}                                                              \
	}) // End I32_VECTOR_EXIST

#define VECTOR_EXIST_S(v)                                                      \
	__extension__({                                                        \
		if (!v) {                                                      \
			VECTOR_EXIST_ERROR(v);                                 \
			return (size_t *)ERR_NO_VECTOR;                        \
		}                                                              \
	}) // End VECTOR_EXIST_S
// #define ADD_OFFSET(val, vec) ((val * vec->type_size)) // End ADD_OFFSET
// ////////////////////////////////

#endif // _I32_VECTOR_H
