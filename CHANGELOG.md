<a name=""></a>
##  (2019-09-18)


#### Bug Fixes

* **i32_vector.h:**  Add guard for macro definition ([2e409b37](2e409b37))
* **tests/vector.c:**  Fix assertion issues ([c194cb2c](c194cb2c))

#### Features

* **CHANGELOG.md:**
  *  Add CHANGELOG.md ([daecb7cb](daecb7cb))
  *  Add CHANGELOG.md ([3ab77c42](3ab77c42))
* **conanfile.py:**
  *  Add built type check for tests ([5e916791](5e916791))
  *  Add Makefile generator ([80b1af50](80b1af50))
