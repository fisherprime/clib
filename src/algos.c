#include <stdio.h>
#include <math.h>

#include "math/algos.h"
#include "util.h"

/**
 * @brief      Calculate GCD using Euclid's algorithm.
 *
 * @details Euclid's algorithm logic:
 * m = qn + r,  if r = 0 then m is a multiple of n, else if r != 0, any number that divides both m &
 * n must divide m-qn=r, &
 * any number that divides both n & r, must divide qn+r=m; a set of divisors of {m,n} is the same as
 *the set of divisors of {n,r}.
 *
 *
 * @param[in]  m_value  The m value
 * @param[in]  n_value  The n value
 *
 * @return     The Greatest Common Divisor
 */
int32_t euclidean_gcd(int32_t m_value, int32_t n_value)
{
	int32_t remainder_value = 0;
	static int32_t calls = 0;

#ifndef NDEBUG
	if (!calls)
		printf("[+] Calculating GCD (Euclid's algorithm)\n");
#endif

	calls++;

	// Sanity check, divide larger by smaller value
	if (n_value > m_value) {
		int32_t temp = m_value;

		m_value = n_value;
		n_value = temp;
	}

	remainder_value = m_value % n_value;

#ifndef NDEBUG
	printf("m = %d"
	       "\nn = %d"
	       "\nr = %d\n\n",
	       m_value, n_value, remainder_value);
#endif

	if (remainder_value == 0)
		return n_value;

	return euclidean_gcd(n_value, remainder_value);
} // End euclidean_gcd

/**
 * @brief      Perform a^b%n operation
 *
 * @details Perform a power operation on variables "left_hs" & "right_hs", then a modulus on the
 *result with "modulus"
 * summary: bc (mod n) => (b (mod n)) * (c (mod n)) * (mod n).
 *
 * @param[in]  left_hs   The left hs
 * @param[in]  right_hs  The right hs
 * @param[in]  modulus   The modulus
 *
 * @return     result of powering & modulus operation
 */
int32_t power_modulus(int32_t left_hs, int32_t right_hs, int32_t modulus)
{
	int32_t result = 1;

	while (right_hs) {
		if (right_hs % BASE_POWER_VALUE)
			result = (result * left_hs) % modulus;

		left_hs = (int32_t)pow(left_hs, BASE_POWER_VALUE) % modulus;
		right_hs /= BASE_POWER_VALUE;
	}

	return result;

	/**
	 * @details Logic behind craziness (Primality test) Now say we want to encrypt the message m =
	 * 7, c = m^e mod n = 7^3 mod 33 = 343 mod 33 =
	 * 13, hence the ciphertext c = 13, to check decryption we compute m' =
	 * c^d mod n = 13^7 mod 33 = 7 note that we don't have to calculate the full value of 13 to the
	 *power 7 here.
	 *
	 * @details We can make use of the fact that a = bc mod n = (b mod n), (c mod n) mod n so we can
	 *break  down a potentially large number into its components and combine the results of easier,
	 *smaller calculations to calculate the final value.
	 *
	 * @details One way of calculating m' is as follows:- Note that any number can be expressed as a
	 *sum of powers of 2. So first compute values of 13^2, 13^4, 13^8, ... by repeatedly squaring
	 *successive values modulo 33.
	 * 13^2 = 169 ≡ 4, 13^4 = 4.4 = 16, 13^8 = 16.16 = 256 ≡ 25.  Then, since 7 = 4 + 2 + 1, we have
	 *m' = 13^7 = 13^(4+2+1) = 13^4.13^2.13^1 ≡ 16 x 4 x 13 = 832 ≡ 7 mod 33
	 */
} // End power_modulus

/**
 * @brief      Convert temperature units.
 *
 * @param[in, out]  in_temp   Input temperature
 * @param[in]  in_type   Input type
 * @param[in]  out_type  Output type
 *
 * @return     converted temperature / NULL on error
 */
double convert_temperature(double *in_temp, int8_t in_type, int8_t out_type)
{
	if (in_type == out_type)
		return (double)SUCCESS;

	double out_temp;

	// @brief Converting temperature to celsius for easier handling.
	switch (in_type) {
	case FARENHEIT:
		out_temp = (5.0 / 9.0) * (*in_temp - 32);

		break;
	case KELVIN:
		if (*in_temp < 0)
			print_error(
				NULL, 0,
				(char *)"temperature_convert: \nTemperature seems impossibly cold");

		out_temp = *in_temp + ZERO_KELVIN;

		break;
	default:
		print_error(
			NULL, 0,
			(char *)"temperature_convert: \nUnknown temperature type to convert from");

		return (double)ERR_UNKNOWN_TEMP_UNIT;
	}

	switch (out_type) {
	case FARENHEIT:
		*in_temp = ((9.0 / 5.0) * out_temp) + 32;

		break;
	case KELVIN:
		*in_temp = out_temp - ZERO_KELVIN;

		break;
	case CELSIUS:
		*in_temp = out_temp;

		break;
	default:
		print_error(
			NULL, 0,
			(char *)"temperature_convert: \nUnknown temperature type to convert to");

		return (double)ERR_UNKNOWN_TEMP_UNIT;
	}

	return (double)SUCCESS;
} // End temperature_convert
