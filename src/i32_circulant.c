#include <string.h>
#include <stdio.h>
#include <math.h>

#include "util.h"
#include "types/matrix/i32/i32_circulant.h"

/**
 * @brief      Creates a circulant matrix.
 *
 * @param      user_matrix  The user matrix
 * @param      user_vector_i32_t  The user vector to create circulant from
 *
 * @return     Exit status
 */
int8_t i32_circulant_create(i32_vector *user_matrix,
			    i32_vector *user_vector_i32_t)
{
	I32_VECTOR_EXIST(user_vector_i32_t);

	int32_t *mtx_pos; // Pointer to start of matrix
	int32_t *vct_data;

	size_t *mtx_size;
	size_t *vct_size;
	size_t m_dimensions = 0;

	vct_data = i32_vector_data(user_vector_i32_t);
	vct_size = i32_vector_size(user_vector_i32_t);
	m_dimensions = *vct_size;

	i32_vector_init(user_matrix, (*vct_size * *vct_size));

	mtx_pos = i32_vector_data(user_matrix);
	mtx_size = i32_vector_size(user_matrix);

	// Replicate rows to create matrix
	for (size_t n_dimensions = *vct_size; n_dimensions > 0;
	     n_dimensions--) {
		size_t counter = m_dimensions - n_dimensions;

		memmove((mtx_pos + (counter * m_dimensions)), vct_data,
			*vct_size * sizeof(int32_t));
		*mtx_size += m_dimensions;
	}

#ifndef NDEBUG
	i32_circulant_print(user_matrix);
	i32_circulant_rotate_matrix_rows(user_matrix, (size_t)m_dimensions);
	i32_circulant_print(user_matrix);
#else

	rotate_matrix_rows(user_matrix, (size_t)m_dimensions);
#endif

	i32_vector_free(user_vector_i32_t);

	return SUCCESS;
} // End create_circulant

/**
 * @brief      Prints a circulant matrix.
 *
 * @param[in]      user_matrix  The user matrix
 *
 * @return     Exit status
 */
int8_t i32_circulant_print(i32_vector *user_matrix)
{
	I32_VECTOR_EXIST(user_matrix);

	int32_t *mtx_pos;
	size_t dimensions = 0;

	mtx_pos = i32_vector_data(user_matrix);
	dimensions = (size_t)sqrt((double)*i32_vector_size(user_matrix));

	printf("[+] Printing circulant matrix\n");

	for (size_t m_counter = 0; m_counter < dimensions; m_counter++) {
		printf("\t[ ");

		for (size_t n_counter = 0; n_counter < dimensions; n_counter++)
			printf("%*" PRIu32 " ", (int32_t)DEFAULT_DIGIT_LENGTH,
			       *(mtx_pos +
				 (m_counter * dimensions + n_counter)));

		printf("]\n");
	}

	return SUCCESS;
} // End print_circulant

/**
 * @brief      Rotate matrix rows to form a circulant matrix.
 *
 * @param[in]      user_matrix  The user matrix
 * @param[in]  dimensions   The dimensions of the user vector
 *
 * @return     Exit status
 */
int8_t i32_circulant_rotate_matrix_rows(i32_vector *user_matrix,
					size_t dimensions)
{
	I32_VECTOR_EXIST(user_matrix);

	int32_t *mtx_pos = i32_vector_data(user_matrix);

	for (size_t m_counter = 0; m_counter < dimensions; m_counter++)
		for (size_t rotation_amnt = m_counter;
		     rotation_amnt > (size_t)0; rotation_amnt--) {
			size_t n_counter = (dimensions - 1);

			int32_t temp = *(mtx_pos +
					 (m_counter * dimensions + n_counter));

// Same thing
#if 0
			uint32_t *temp = (uint32_t *) calloc(1, sizeof(uint32_t));
			memcpy(temp, (mtx_pos + (ADD_OFFSET(m_counter, user_matrix) *
			                         dimensions + n_counter)), ADD_OFFSET(1, user_matrix));
#endif

			while (n_counter > (size_t)0) {
				*(mtx_pos + (m_counter * dimensions) +
				  n_counter) =
					*(mtx_pos + (m_counter * dimensions) +
					  (n_counter - 1));

				n_counter--;
			}

			*(mtx_pos + (m_counter * dimensions)) = temp;
		}

	return SUCCESS;
} // End rotate_matrix_rows
