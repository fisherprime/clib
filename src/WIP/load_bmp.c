#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "util.h"
#include "io/load_bmp.h"

int load_bmp_file(char *image_path)
{
	// Data read from the header of the BMP file
	size_t image_width;
	size_t image_height;
	size_t image_size; // width*height*3

	unsigned char *header; // Each BMP file begins by a 54-bytes header
	unsigned int data_position; // File position where the actual data begins
	unsigned char *data; // Actual RGB data

	header = (unsigned char *)calloc(54, sizeof(unsigned char));

	// Vulnerable to symlink redirections
	FILE *bmp_image = fopen(image_path, "rb");

	if (!bmp_image) {
		print_error((char *)"fopen", 1,
			    (char *)"Image could not be opened");

		LOGGING_FREE(header);

		return ERR_BMP_FILE_OPEN;
	}

	/**
	 * Test BMP-ness If not 54 bytes read : problem
	 */
	if (fread(header, 1, 54, bmp_image) != 54) {
		print_error((char *)"fread", 1, (char *)"Not a valid BMP file");

		LOGGING_FREE(header);
		fclose(bmp_image);

		return ERR_NOT_BMP_FILE;
	}

	if ((header[0] != 'B') || (header[1] != 'M')) {
		print_error((char *)"FILE", 1, (char *)"Not a valid BMP file");

		LOGGING_FREE(header);
		fclose(bmp_image);

		return ERR_NOT_BMP_FILE;
	}

	// Make sure this is a 24bpp file
	if ((*(int *)&(header[0x1E]) != 0) || (*(int *)&(header[0x1C]) != 24)) {
		print_error((char *)"FILE", 1,
			    (char *)"Not a correct BMP file");

		LOGGING_FREE(header);
		fclose(bmp_image);

		return ERR_NOT_BMP_FILE;
	}

	/**
	 * Read integer values from the byte array
	 */
	data_position = *(int *)&(header[0x0A]);
	image_size = *(int *)&(header[0x22]);
	image_width = *(int *)&(header[0x12]);
	image_height = *(int *)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information 3 : one
	// byte
	// for each Red, Green and Blue component
	if (image_size == (size_t)0)
		image_size = image_width * image_height * 3;

	// The BMP header is done that way
	if (data_position == (uint8_t)0)
		data_position = 54;

	data = (unsigned char *)calloc(image_size, sizeof(char));

	// Read the actual data from the file into the buffer
	fread(data, 1, image_size, bmp_image);

	// Everything is in memory now, the file can be closed.
	fclose(bmp_image);

	// Create one OpenGL texture
	uint texture_id;

	glGenTextures(1, &texture_id);

	// "Bind" the newly created texture : all future texture functions
	// will
	// modify this texture
	glBindTexture(GL_TEXTURE_2D, texture_id);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imaage_width, image_height, 0,
		     GL_BGR, GL_UNSIGNED_BYTE, data);

	// OpenGL has now copied the data. Free our own version
	free(data);

	// Poor filtering, or ... glTexParameteri(GL_TEXTURE_2D,
	// GL_TEXTURE_MAG_FILTER, GL_NEAREST); glTexParameteri(GL_TEXTURE_2D,
	// GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	// ... nice trilinear filtering ...
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			GL_LINEAR_MIPMAP_LINEAR);
	// ... which requires mipmaps. Generate them automatically.
	glGenerateMipmap(GL_TEXTURE_2D);

	LOGGING_FREE(header);

	// Return the ID of the texture we just created
	return texture_id;
} // End load_bmp_file
