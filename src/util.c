#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#include "util.h"

// To avoid multiple definition issues
execution_state g_state;

/**
 * @brief      Perform cleanup on code operations & residual data.
 */
inline void cleanup(void)
{
	if (cleanup_files())
		print_error(
			NULL, 1,
			(char *)"my_cleanup: \n\tAn error occurred while calling fd_cleanup");

	LOGGING_FREE(g_state.sigaction);
} // End my_cleanup

inline void default_custom_strncat(char *dest, char *src)
{
	strncat(dest, (const char *)src, STRING_REM_DEF(dest));
}

inline void custom_strncat(char *dest, char *src, size_t len)
{
	size_t str_free_bytes = STRING_REM_DEF(dest);

	/**
	 * @note If len <= amount of empty bytes in a string, use len, else use ammount of empty bytes
	 */
	str_free_bytes = MIN(len, str_free_bytes);
	strncat(dest, (const char *)src, str_free_bytes);
}

inline void default_custom_strncpy(char *dest, char *src)
{
	strncpy(dest, (const char *)src, DEFAULT_STRING_LEN);
}

inline void custom_strncpy(char *dest, char *src, size_t len)
{
	/**
	 * @note If len <= predetermined default string length, use len else use predetermined length
	 */
	strncpy(dest, (const char *)src, MIN(len, (DEFAULT_STRING_LEN - 1)));
}

inline void default_custom_snprintf(char *dest, char *fmt_str, ...)
{
	va_list vl;

	// Specify starting point of variable arg list by specifying the last
	// argument before it
	va_start(vl, fmt_str);
	vsnprintf(dest, (DEFAULT_STRING_LEN - 1), (const char *)fmt_str, vl);
	va_end(vl);
}

inline void custom_snprintf(char *dest, size_t len, char *fmt_str, ...)
{
	va_list vl;

	va_start(vl, fmt_str);
	vsnprintf(dest, MIN(len, (DEFAULT_STRING_LEN - 1)),
		  (const char *)fmt_str, vl);
	va_end(vl);
}

/**
 * @brief      Notify handling of specific signals.
 *
 * @param[in]  signal  The signal
 *
 * @return     Exit status
 */
void _print_signal_handler_message(char *signal)
{
	size_t message_length = HALF_STRING_LEN + STRING_LEN_DEF(signal);

	char *message = (char *)calloc(message_length + 1, sizeof(char));

	switch (g_state.process_type) {
	case CLIENT_PROCESS_ID:
		snprintf(message, message_length,
			 "Client process received %s, exiting...", signal);

		break;
	case SERVER_PROCESS_ID:
		snprintf(message, message_length,
			 "Server process received %s, exiting...", signal);

		break;
	default:
		LOGGING_FREE(message);

		break;
	}

	print_error(NULL, 0, message);

	LOGGING_FREE(message);
} // End int _print_signal_handler_message

/**
 * @brief      General purpose signal handler.
 *
 * @brief      In the code create and populate a sigaction stuct specify the signal handler, and the
 *signals it should handle.
 *
 * @param[in]  signal_id  The signal identifier
 */
void signal_handler(int signal_id)
{
	switch (signal_id) {
	case SIGABRT:
		/**
			 * @details	This signal indicates an error detected by the program itself and reported
			 *by calling abort, Occurs mostly as a result of assertion failures due to memory
			 *allocation errors.
			 *
			 */
		_print_signal_handler_message((char *)"SIGABRT");

		cleanup();
		exit(ERR_SIGABRT);

		break;
	case SIGTERM:
		_print_signal_handler_message((char *)"SIGTERM");

		cleanup();
		exit(ERR_SIGTERM);

		break;
	case SIGINT:
		// Ctrl + C interrupt received
		_print_signal_handler_message((char *)"SIGINT");

		// Doesn't always terminate
		g_state.quit = YES;

		if (g_state.process_type != SERVER_PROCESS_ID) {
			cleanup();
			exit(ERR_SIGINT);
		}

		break;
	case SIGSEGV:
		/**
			 * @details This signal is generated when a program tries to read or write outside the
			 *memory that is allocated for it, or to write memory that can only be read.  (Actually,
			 *the signals only occur when the program goes far enough outside to be detected by the
			 *system’s memory protection mechanism.) The name is an abbreviation for “segmentation
			 *violation”.
			 */
		_print_signal_handler_message((char *)"SIGSEGV");

		cleanup();
		exit(ERR_SIGSEGV);

		break;
	case SIGILL:
		/**
			 * @details The name of this signal is derived from “illegal instruction”;
			 * it usually means your program is trying to execute garbage or a privileged
			 *instruction. It can also be generated when the stack overflows, or when the system has
			 *trouble running the handler for a signal.
			 */
		_print_signal_handler_message((char *)"SIGILL");

		cleanup();
		exit(ERR_SIGILL);

		break;
	case SIGBUS:
		/**
			 * @details	This signal is generated when an invalid pointer is dereferenced. Like
			 *SIGSEGV, this signal is typically the result of dereferencing an uninitialized
			 *pointer.  The difference between the two is that SIGSEGV indicates an invalid access
			 *to valid memory, while SIGBUS indicates an access to an invalid address.
			 */
		if (g_state.process_type == CLIENT_PROCESS_ID)
			print_error(
				NULL, 0,
				(char *)"Client process received SIGILL, exiting...");
		else
			print_error(
				NULL, 0,
				(char *)"Server process received SIGILL, exiting...");

		cleanup();
		exit(ERR_SIGBUS);

		break;
	default:
		break;
	}
} // End my_signal_handler

/**
 * @brief      Setup common stuff.
 */
void setup(void)
{
	g_state.process_type = UNDEFINED_PROCESS_ID;
	g_state.quit = NO;
	g_state.verbose = NO;

	// Setup socket handling
	{
		g_state.accept_sockets =
			(i32_vector *)calloc(1, sizeof(i32_vector));
		g_state.open_files =
			(i32_vector *)calloc(1, sizeof(i32_vector));
		g_state.std_sockets =
			(i32_vector *)calloc(1, sizeof(i32_vector));

#if 0
		g_state.accept_sockets->type_size = sizeof(int);
		g_state.open_files->type_size = sizeof(int);
		g_state.std_sockets->type_size = sizeof(int);
#endif
	}

	// Set seed for random number generation
	srand(time(NULL));

	// Setup signal handling
	{
		g_state.sigaction =
			(struct sigaction *)calloc(1, sizeof(struct sigaction));

		g_state.sigaction->sa_handler = signal_handler;
		sigemptyset(&g_state.sigaction->sa_mask);
		g_state.sigaction->sa_flags = 0;

		// Third parameter is a struct defining the old action
		sigaction(SIGABRT, g_state.sigaction, NULL);
		sigaction(SIGINT, g_state.sigaction, NULL);
		sigaction(SIGSEGV, g_state.sigaction, NULL);
		sigaction(SIGTERM, g_state.sigaction, NULL);
	}
} // End my_setup

/**
 * @brief      Cleanup active file descriptors on signal.
 *
 * @brief      Will work all UNIX file descriptors,they are saved in a C vector and are closed from
 *the most recently opened.
 *
 * @return     Status on exit
 */
int8_t cleanup_files(void)
{
	int fd = 0;

	size_t accept_vct_size;
	size_t std_vct_size;
	size_t open_fd_size;

	char *temp = (char *)calloc(DEFAULT_STRING_LEN, sizeof(char));

#if 0
	vector_details(g_state.open_files);
	vector_details(g_state.accept_sockets);
	vector_details(g_state.std_sockets);
#endif

	open_fd_size = *i32_vector_size(g_state.open_files);

#ifndef NDEBUG
	snprintf(temp, DEFAULT_STRING_LEN, "Closing [%zu file descriptor(s)\n",
		 open_fd_size);
	print_error(NULL, 0, (char *)temp);
#endif

	for (size_t iter = 0; iter < open_fd_size; iter++)
		CLOSE_FD(fd, temp, g_state.open_files, iter);

	i32_vector_free(g_state.open_files);

	accept_vct_size = *i32_vector_size(g_state.accept_sockets);

#ifndef NDEBUG
	memset(temp, 0, DEFAULT_STRING_LEN);
	snprintf(temp, DEFAULT_STRING_LEN, "Closing [%zu accepting socket(s)\n",
		 open_fd_size);
	print_error(NULL, 0, (char *)temp);
#endif

	for (size_t iter = 0; iter < accept_vct_size; iter++)
		CLOSE_FD(fd, temp, g_state.accept_sockets, iter);

	i32_vector_free(g_state.accept_sockets);

	std_vct_size = *i32_vector_size(g_state.std_sockets);

#ifndef NDEBUG
	memset(temp, 0, DEFAULT_STRING_LEN);
	snprintf(temp, DEFAULT_STRING_LEN, "Closing [%zu standard socket(s)",
		 open_fd_size);

	print_error(NULL, 0, (char *)temp);
#endif

	for (size_t iter = 0; iter < std_vct_size; iter++)
		CLOSE_FD(fd, temp, g_state.std_sockets, iter);

	i32_vector_free(g_state.std_sockets);
	LOGGING_FREE(temp);

	/**
	 * @brief      Sockets may be held by the kernel for a short while after closing, this allows
	 *for TCP cleanup on server bound sockets.
	 */
	if (g_state.process_type == SERVER_PROCESS_ID) {
		struct timespec s = { .tv_sec = (time_t)1, .tv_nsec = 0L };
		delay(&s);
	}

	return SUCCESS;
} // fd_cleanup

/**
 * @brief Implement a delay in code execution.
 *
 * @param wait_time A timespec specifying the delay period.
 */
inline void delay(struct timespec *wait_time)
{
	nanosleep(wait_time, NULL);
} // End function_delay

/**
 * @brief     	Populate usage flags.
 *
 * @brief       All arguments are mandatory Create an array of characters|c_string to hold the
 *flags. Pass a sequence of characters to represent a user choice.
 *
 * @param      flags             The flags
 * @param      insertion_string  The insertion string
 */
inline void populate_usage_flags(char *flags, char *insertion_string)
{
	if (flags)
		default_custom_strncat(flags, insertion_string);
} // End populate_usage_flags

/**
 * @brief      	Print descriptive error messages.
 *
 * @brief       All arguments are mandatory. Call the function as print_error (function_name,
 *message, indent_level) Where: <function_name> is the name of the error producing function
 *<message> is a user defined message
 *
 * @param      function  The function failing
 * @param      message   The user-defined error message
 * @param[in]  level     The "numbering" level of the message
 */
void print_error(char *function, int level, char *message, ...)
{
	va_list vl;

	if (!function && !message) {
		fprintf(stderr,
			"[!] print_error: received no function nor message");

		return;
	}

	if (level == 0)
		fprintf(stderr, "\n");

	for (; level > 0; level--)
		fprintf(stderr, "\t");

	va_start(vl, message);
	fprintf(stderr, "[!] ");
	vfprintf(stderr, (const char *)message, vl);
	fprintf(stderr, "\n"); // This is needed, obviously
	va_end(vl);

	if (function) {
		size_t string_length = 5 + STRING_LEN_DEF(function) + 1;

		char *perror_string =
			(char *)calloc(string_length, sizeof(char));

		custom_snprintf(perror_string, string_length,
				(char *)"\t[!] %s", function);
		perror(perror_string);

		LOGGING_FREE(perror_string);
	}
} // End print_error

/**
 * @brief      Provide hex dump of some arbitrary memory
 *
 * @brief      source: http://stackoverflow.com/a/7776146, user:
 * https://stackoverflow.com/users/14860/paxdiablo with inspiration from flatcc/support/hexdump.h
 *
 * @param      description  The description
 * @param      address      The address
 * @param[in]  length       The length
 * @param      file_ptr     The file pointer
 */
void hex_dump(char *description, void *address, int32_t length, FILE *file_ptr)
{
	int iter = 0;
	unsigned char buffer[HEX_STRING_LEN + 1];
	unsigned char *character = (unsigned char *)address;

	// Output description if given.
	if (description != NULL)
		fprintf(file_ptr, "\n[+] %s:\n", description);

	if (length == 0) {
		fprintf(file_ptr, "\t[!] zero length\n");

		return;
	}

	if (length < 0) {
		fprintf(file_ptr, "\t[!] Negative length: %" PRId32 "\n",
			length);

		return;
	}

	// Process every byte in the data.
	for (iter = 0; iter < length; iter++) {
		// Multiple of 16 means new line (with line offset).

		if ((iter % 16) == 0) {
			// Don't print ASCII for the zeroth line.
			if (iter)
				fprintf(file_ptr, "  %s\n", buffer);

			// Output the offset.
			fprintf(file_ptr, "  %04x ", iter);
		}

		// Now the hex code for the specific character.
		fprintf(file_ptr, " %02x", character[iter]);

		// And store a printable ASCII character for later.
		if ((character[iter] < 0x20) || (character[iter] > 0x7e))
			buffer[iter % 16] = '.';
		else
			buffer[iter % 16] = character[iter];

		buffer[(iter % 16) + 1] = '\0';
	} // End for

	// Pad out last line if not exactly 16 characters.
	for (; (iter % 16) != 0; iter++)
		fprintf(file_ptr, "   ");

	// And print the final ASCII bit.
	fprintf(file_ptr, "  %s\n", buffer);
	fprintf(file_ptr, "\nLength: %" PRId32 "\n\n", length);
} // End hex_dump

/**
 * @brief      Shows the percentage progress of tasks.
 *
 * @param[in]  current_percentage  The current percentage
 * @param[in]  max_percentage      The maximum percentage
 */
// void show_progress (int8_t current_percentage, int8_t max_percentage) {
//      printf ()
// } // End show_progress

/**
 * @brief      The Tao of Programming.
 */
void sudo()
{
	// 020E9B6378A5F22E2E7A9F03E87F890DD82266B7
	int8_t input_argument;
	int32_t long_opts_index = 0;

	struct option long_opts[] = { { "lost", no_argument, 0, 0 },
				      { 0, 0, 0, 0 } };

	while ((input_argument = getopt_long(g_state.argc, g_state.argv, "0",
					     long_opts, &long_opts_index)) !=
	       -1) {
		if (input_argument == 0)
			printf("\n" WISDOM);
	}
} // End sudo
